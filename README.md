# vue-json-to-table
Creating a VueJs application which fetches JSON data from API and displays a table with the contents. Additional filters are possible using buttons, calling different API endpoints.

# Article @ blog
Check my relevant [blog article here](https://siposm.hu/blog/vuejs-json-to-table) about this project to better understand what is going on.

direct link: https://siposm.hu/blog/vuejs-json-to-table


# Real application
The fully working, deployed application can be [reached here](https://siposm.hu/demo/vue-json-to-table/) for testing.

direct link: https://siposm.hu/demo/vue-json-to-table/